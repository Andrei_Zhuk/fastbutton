<?php

/**
 * Webbuilder videolink API
 *
 */
class Api_Fastbutton_Fastbutton extends Api_Service_Abstract
{

    protected $_accessList
        = array(
            Tools_Security_Acl::ROLE_USER       => array('allow' => array('get', 'post', 'put', 'delete')),
            Tools_Security_Acl::ROLE_SUPERADMIN => array('allow' => array('get', 'post', 'put', 'delete')),
            Tools_Security_Acl::ROLE_ADMIN      => array('allow' => array('get', 'post', 'put', 'delete'))
        );

    public function init()
    {
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper        = Application_Model_Mappers_ContainerMapper::getInstance();
        $this->pageMapper     = Application_Model_Mappers_PageMapper::getInstance();
    }

    public function postAction()
    {
        // find page id by link
        $mapper = $this->pageMapper->findByUrl($this->_request->getParam('link'));
        if ($mapper) {
            $linkId = $mapper->getId();
            if (!$linkId) {
                $linkId = "";
            }
        } else {
            $linkId = "";
        }


        $fbData = array(
            'link'   => filter_var($this->_request->getParam('link'), FILTER_SANITIZE_STRING),
            'text'   => filter_var($this->_request->getParam('text'), FILTER_SANITIZE_STRING),
            'target' => filter_var($this->_request->getParam('target'), FILTER_SANITIZE_STRING),
            'class'  => filter_var($this->_request->getParam('class'), FILTER_SANITIZE_STRING),
            'title'  => filter_var($this->_request->getParam('title'), FILTER_SANITIZE_STRING),
            'linkId' => filter_var($linkId, FILTER_SANITIZE_STRING)
        );

//        $tokenToValidate = $this->_request->getParam(Tools_System_Tools::CSRF_SECURE_TOKEN, false);
//        $valid = Tools_System_Tools::validateToken($tokenToValidate, Webbuilder::WB_VIDEOLINK_SECURE_TOKEN);
//        if (!$valid) {
//            exit;
//        }

        $containerName = filter_var($this->_request->getParam('container'), FILTER_SANITIZE_STRING);
        $pageId        = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);
        $type          = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container     = $this->_mapper->findByName($containerName, $pageId, $type);

        if ($pageId == 0) {
            $pageId = null;
        }

        if (!$container instanceof Application_Model_Models_Container) {
            $container = new Application_Model_Models_Container();
            $container->setName($containerName)
                      ->setPageId($pageId)
                      ->setContainerType(
                          ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT
                      );
        }
        $container->setContent(Zend_Json::encode($fbData));

        try {
            return array('error' => false, 'responseText' => $this->_mapper->save($container));
        } catch (Exception $e) {
            return $this->_error($e->getMessage());
        }
    }

    public function getAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }
}