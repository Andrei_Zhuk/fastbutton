<?php

class Widgets_Fastbutton_Fastbutton extends Widgets_AbstractContent
{
    protected $_cacheable = false;

    protected $_websiteHelper;
    protected $_sessionHelper;

    protected $_buttonName;
    protected $_buttonType      = self::TYPE_REGULAR;
    protected $_buttonClassList = null;
    protected $_buttonLink      = '#';
    private   $_typeList
                                = array(
            '0' => 'None', '1' => '_blank', '2' => '_parent', '3' => '_self', '4' => '_top'
        );

    const TYPE_REGULAR = 1;
    const TYPE_STATIC = 2;

    protected function _init()
    {
        // Set helpers
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('website');
        $this->_sessionHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('session');

        $this->_view = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $this->_view->addScriptPath($this->_websiteHelper->getPath() . 'seotoaster_core/application/views/scripts/');
    }

    protected function _load()
    {
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate(
                    'Not enough parameters for the widget.'
                )
            );
        }

        // Create a class of html-tag with prefix and name of widget
        $name   = 'fb_' . array_shift($this->_options);
        // if we don't have static, we set id of page
        $pageId = (in_array('static', $this->_options)) ? 0 : $this->_toasterOptions['id'];

        if (in_array('static', $this->_options)) {
            $type = Application_Model_Models_Container::TYPE_STATICCONTENT;
            unset($this->_options[array_search('static', $this->_options)]);
        } else {
            $type = Application_Model_Models_Container::TYPE_REGULARCONTENT;
        }
        // Find container by params
        $container = Application_Model_Mappers_ContainerMapper::getInstance()->findByName($name, $pageId, $type);

        $ioData = null;

        if ($container instanceof Application_Model_Models_Container) {
            $ioData = Zend_Json::decode($container->getContent());

            if (is_array($ioData) && !empty($ioData)) {
                foreach ($ioData as $key => $value) {
                    $this->_view->$key = $value;
                }
            }
        }

        // Find data of this container
        $pageMapper = Application_Model_Mappers_PageMapper::getInstance()->find($ioData['linkId']);

        if ($pageMapper instanceof Application_Model_Models_Page && method_exists(Application_Model_Models_Page, 'getPageFolder')) {
            $link = $ioData['link'];
            if(!empty($pageMapper->getPageFolder())){
                $link = $this->_toasterOptions['websiteUrl'] . $pageMapper->getPageFolder() . '/' . $pageMapper->getUrl();   
            }
        } else {
            $link = $ioData['link'];

        }



        $this->_buttonClassList     = array_shift($this->_options);
        $this->_view->websiteUrl    = $this->_toasterOptions['websiteUrl'];
        $this->_view->containerName = $name;
        $this->_view->name          = str_replace(' ', '-', $name);
//        $this->_view->link          = isset($ioData['link']) ? $ioData['link'] : '';
        $this->_view->link      = $link;
        $this->_view->text      = isset($ioData['text']) ? $ioData['text'] : '';
        $this->_view->title     = isset($ioData['title']) ? $ioData['title'] : '';
        $this->_view->classList = (isset($ioData['class']) ? $ioData['class'] : '') . ' ' . $this->_buttonClassList;
        $this->_view->target    = ($ioData['target'] != '0' &&  isset($ioData['text']) ) ? $this->_typeList[$ioData['target']] : 0;
        $this->_view->pageId    = $pageId;

        return $this->_view->render('button.phtml');
    }
}