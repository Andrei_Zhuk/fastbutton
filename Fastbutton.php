<?php

class Fastbutton extends Tools_Plugins_Abstract
{
    private $_layout = null;

    private $_typeList = array( '0' => 'None', '1' => '_blank', '2' => '_parent', '3' => '_self', '4' => '_top' );


    protected function _init()
    {
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        $this->_view = new Zend_View(array( 'scriptPath' => __DIR__ . '/views' ));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        if (($scriptPaths = $this->_layout->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');

        $this->_isSAdmin = $this->_sessionHelper->getCurrentUser()->getRoleId() == Tools_Security_Acl::ROLE_SUPERADMIN ? true : false;
    }

    public function run($requestedParams = array())
    {
        $this->_requestedParams = $requestedParams;
        parent::run($requestedParams);

    }

    public function editConfigAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $containerName = filter_var($this->_request->getParam('container'), FILTER_SANITIZE_STRING);
            $pageId        = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);
            $type          = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
            $container     = Application_Model_Mappers_ContainerMapper::getInstance()->findByName(
                $containerName,
                $pageId,
                $type
            );
            $ioData        = null;

            if ($container instanceof Application_Model_Models_Container) {
                $ioData = Zend_Json::decode($container->getContent());
            }


            $pageMapper = Application_Model_Mappers_PageMapper::getInstance()->find($ioData['linkId']);

            if ($pageMapper) {
                $link = $pageMapper->getUrl();
            } else {
                $link = $ioData['link'];
            }

            // assign view variables
            $this->_view->content       = ($container instanceof Application_Model_Models_Container) ? explode(
                ':',
                $container->getContent()
            ) : '';
            $this->_view->pageId        = $pageId;
            $this->_view->containerName = $containerName;
            $this->_view->typeList      = $this->_typeList;
            $this->_view->link          = $link;
            $this->_view->text          = isset($ioData['text']) ? $ioData['text'] : '';
            $this->_view->title         = isset($ioData['title']) ? $ioData['title'] : '';
            $this->_view->classList     = isset($ioData['class']) ? $ioData['class'] : '';
            $this->_view->target        = isset($ioData['target']) ? $ioData['target'] : 0;
            $this->_view->websiteUrl    = $this->_seotoasterData['websiteUrl'];
            $this->_view->isSAdmin       = $this->_isSAdmin;
            $this->_view->pagesList     = Application_Model_Mappers_PageMapper::getInstance()->fetchAll(
                null,
                array('h1 ASC')
            );

            echo $this->_view->render('edit.config.phtml');
        }
    }

}